import React from 'react';
import ReactPropTypes from 'prop-types';
// Components
import Svg from '_core/components/elements/Svg';
// Utils
import classnames from 'classnames';

class FileIcon extends React.Component {
    static displayName = 'FileIcon';

    static propTypes = {
        theme: ReactPropTypes.string,
        size: ReactPropTypes.string,
        svgName: ReactPropTypes.string,
        classMod: ReactPropTypes.string,
    };

    static defaultProps = {
        theme: 'light',
        size: '',
        svgName: '',
        classMod: '',
    };

    fileIconsClasses = ({ theme, size, classMod }) => {
        return classnames({
            'file-icon': true,
            [`file-icon_size_${size}`]: size,
            [`file-icon_theme_${theme}`]: theme,
            [classMod]: classMod,
        });
    };

    render() {
        const { theme, size, svgName, classMod } = this.props;

        return (
            <span className={this.fileIconsClasses({ theme, size, classMod })}>
                <Svg name={svgName} />
            </span>
        );
    }
}

export default FileIcon;
