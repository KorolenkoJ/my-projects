import React from 'react';
import ReactPropTypes from 'prop-types';
import classnames from 'classnames';
// Mobx
import { observer } from 'mobx-react';
import { observable } from 'mobx';
// Store
import store from '_core/store';
// Components
import Svg from '_core/components/elements/Svg';
import Link from '_core/components/elements/link/Link';
import LinkText from '_core/components/elements/link/LinkText';
import LinkIcon from '_core/components/elements/link/LinkIcon';
import Tooltip from '_core/components/elements/Tooltip';
import SwitchLang from '_core/components/elements/SwitchLang';

class LangTooltip extends React.Component {
    static displayName = 'LangTooltip';

    static propTypes = {
        theme: ReactPropTypes.string,
        color: ReactPropTypes.string,
        placement: ReactPropTypes.string,
        left: ReactPropTypes.number,
        top: ReactPropTypes.number,
    };

    static defaultProps = {
        theme: 'dark',
        color: 'dark',
        placement: 'top',
        left: 0,
        top: 0,
    };

    isVisibleTooltip = observable.box(false);

    handleTooltipVisibility = (isVisible) => {
        return this.isVisibleTooltip.set(isVisible);
    };

    langMenuClasses = ({ state, color }) => {
        return classnames({
            'lang-link': true,
            [`lang-link_theme_${color}`]: color,
            [`lang-link_state_${state}`]: state,
        });
    };

    langTooltipClasses = ({ theme }) => {
        return classnames({
            tooltip_type_list: true,
            [`tooltip_theme_${theme}`]: theme,
        });
    };

    render() {
        const {
            theme,
            color,
            placement,
            left,
            top,
            anchorClassMod,
        } = this.props;

        return (
            <Tooltip
                classMod={this.langTooltipClasses({ theme })}
                anchorClassMod={anchorClassMod}
                placement={placement}
                align={[left, top]}
                visible={this.isVisibleTooltip.get()}
                hideTooltip={() => this.handleTooltipVisibility(false)}
                onVisibleChange={() =>
                    this.handleTooltipVisibility(!this.isVisibleTooltip.get())
                }
                trigger="click"
                overlay={
                    <SwitchLang
                        hideTooltip={() => this.handleTooltipVisibility(false)}
                        theme={theme}
                    />
                }
            >
                <Link
                    theme=""
                    classMod={this.langMenuClasses({
                        color,
                        state: this.isVisibleTooltip.get() && 'open',
                    })}
                >
                    <LinkText classMod="margin_right_5">
                        {store.lang.value}
                    </LinkText>
                    <LinkIcon width={10} height={10} pos="">
                        <Svg
                            classMod="svg_pos_abs menu__ico"
                            name="caret-bottom"
                        />
                    </LinkIcon>
                </Link>
            </Tooltip>
        );
    }
}

export default observer(LangTooltip);
