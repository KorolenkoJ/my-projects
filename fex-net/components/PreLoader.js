import React from 'react';
import ReactPropTypes from 'prop-types';
import classnames from 'classnames';

class PreLoader extends React.PureComponent {
    static displayName = 'PreLoader';

    static propTypes = {
        theme: ReactPropTypes.string,
        size: ReactPropTypes.string,
        type: ReactPropTypes.string,
    };

    static defaultProps = {
        theme: 'primary',
        size: '',
        type: '',
    };

    classes = ({ theme, size, type }) => {
        return classnames({
            preloader: true,
            [`preloader_theme_${theme}`]: theme,
            [`preloader_size_${size}`]: size,
            [`preloader_type_${type}`]: type,
        });
    };

    render() {
        const { theme, size, type } = this.props;

        return (
            <span
                className={this.classes({
                    theme,
                    size,
                    type,
                })}
            >
                <span className="preloader__item" />
                <span className="preloader__item" />
                <span className="preloader__item" />
                <span className="preloader__item" />
                <span className="preloader__item" />
                <span className="preloader__item" />
                <span className="preloader__item" />
            </span>
        );
    }
}

export default PreLoader;
