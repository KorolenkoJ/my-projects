import React from 'react';
import { observer } from 'mobx-react';
import ReactPropTypes from 'prop-types';

// Utils
import classnames from 'classnames';

class ProgressBar extends React.Component {
    static displayName = 'ProgressBar';

    static propTypes = {
        bgTheme: ReactPropTypes.string,
        value: ReactPropTypes.number,
        valueTheme: ReactPropTypes.string,
        size: ReactPropTypes.string,
        classMod: ReactPropTypes.string,
    };

    static defaultProps = {
        bgTheme: 'white',
        valueTheme: 'primary',
        size: '',
        value: 0,
        classMod: '',
    };

    progressBarClasses = ({ bgTheme, valueTheme, size, value, classMod }) => {
        return classnames({
            'progress-bar': true,
            [`progress-bar_size_${size}`]: size,
            [`progress-bar_theme_${bgTheme}`]: bgTheme,
            [classMod]: classMod,
        });
    };

    progressBarValueClasses = ({ valueTheme }) => {
        return classnames({
            'progress-bar__value': true,
            [`progress-bar__value_theme_${valueTheme}`]: valueTheme,
        });
    };

    render() {
        const { bgTheme, valueTheme, size, value, classMod } = this.props;

        return (
            <div
                className={this.progressBarClasses({ bgTheme, size, classMod })}
            >
                <div
                    className={this.progressBarValueClasses({ valueTheme })}
                    style={{ width: value <= 100 ? `${value}%` : '100%' }}
                />
            </div>
        );
    }
}

export default observer(ProgressBar);
