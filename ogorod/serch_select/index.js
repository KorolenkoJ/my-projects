import dom from "lib/dom";
import {atom, swap, deref} from "lib/atom";
import {KEY, trim} from "lib/helpers";


let a_state = atom({
    'a_pos': 0,
});

let setInput = (el, val, f_input_blur) => {
    let parent = el.closest(".b-species__criteria-search");
    let input = dom.find(parent, ".b-search__input");
    input.value = val;
    if(f_input_blur) {
        input.blur();
    }
};

let hideList = () => {
    dom.removeClass(dom.find(".b-species__search-res_open"), "b-species__search-res_open");
};

let sanitize_value = (value) => {
    return trim(value).toLowerCase();
};

let onkeyup = (event) => {
    let dd = dom.find(".b-species__search-res_open");

    if (!dd) {
        return true;
    }

    if (event.which === KEY.ESC) {
        hideList();
        setInput(dd, "", true);
    }
};


export default (serch_select, func) => {
    if (!serch_select) {
        return true;
    }

    dom.findAll(".js-hidden_input").forEach((el) => {
        el.value = "";
    });

    let el_collection = dom.findAll(serch_select);

    el_collection.forEach(function(item, i){
        item.setAttribute("id", "search_select"+i);
    });


    dom.listen(serch_select, "keyup", (ev) => {
        let el = ev.target.closest(".b-search__input");
        const val = sanitize_value(el.value);
        let curId = ev.target.closest(serch_select).id;

        let curSet = deref(a_state)[curId];
        let curLength = deref(a_state).curLength;

        for(var i=0; i<curLength; i++) {
            const res_val = sanitize_value(curSet[i].innerHTML);

            if(res_val.indexOf(val) > -1) {
                dom.removeClass(curSet[i], "b-search-res__hidden");
            }
            else {
                dom.addClass(curSet[i], "b-search-res__hidden");
            }
        };
        onkeyup(ev);

    });

    dom.listen(serch_select, "focus", (ev) => {

        let parent = ev.target.closest(serch_select);
        let search_res = dom.find(parent, ".b-species__search-res");
        let search_res_item = dom.findAll(search_res, ".b-search-res__item");
        let curId = parent.id;

    //Show full list on focus
        for(var i=0; i<search_res_item.length; i++) {
            dom.removeClass(search_res_item[i], "b-search-res__hidden");
        };

    //Save in state current data of element
        swap(a_state, (current) => {

            for(var key in current) {
                if (key === curId) {
                    break;
                }
                else {
                    current[curId] = search_res_item;
                    current['curLength'] = search_res_item.length;
                    current['a_pos'] = 0;
                }
            }
            return current;
        });

        dom.addClass(search_res, "b-species__search-res_open");
        setInput(ev.target, "", false);
    });

    dom.listen(".b-search-res__item", "click", (ev) => {
        let curEl = ev.target;
        let input_hidden = dom.find(curEl.closest(".b-species__criteria-search"), ".js-hidden_input");
        let cur_data = curEl.getAttribute("data-id");

        dom.removeClass(dom.find(".b-species__res-active"), "b-species__res-active");
        dom.addClass(curEl, "b-species__res-active");

        input_hidden.value = cur_data;

        hideList();
        setInput(curEl, curEl.innerHTML, false);
        func(cur_data, ev);
    });


    document.body.addEventListener("click", function(ev) {
        if(ev.target.className !== "b-search-res__item" && ev.target.className !== "b-search__input b-species__input") {
            hideList();

            if(dom.find(".b-species__res-active")) {
                let last_val = dom.find(".b-species__res-active").innerHTML;
                setInput(dom.find(".b-species__res-active"), last_val, true);
            }
        }
    });

}
