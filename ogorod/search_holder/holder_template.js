export default (suggest) => {
    return ["div", {"class": "b-holder"},
                ["div", {"class": "b-holder__item"},
                    ["span", {"data-id": `${suggest.id}`}, `${suggest.name}`]],
                ["span", {"class": "b-holder__remove i-close"}]];
};
