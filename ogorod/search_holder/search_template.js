import dom from "lib/dom";
import fp from "lib/fp";

let item_template = (item) => {
    return ["li", {"class": "b-search-res__item b-holder__search-res-item", "data-search-id": `${item.id}`, "data-search-name": `${item.name}`}, `${item.name}`];
};

let search_template = function(suggest)  {
    return [ "div", {"class": "b-search-res b-holder__search-res"}, ["ul", {"class": "b-search-res__list"}].concat(suggest.map(item_template)) ];
};

export default search_template;
