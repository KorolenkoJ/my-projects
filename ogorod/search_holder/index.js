import fp from "lib/fp";
import dom from "lib/dom";
import form from "lib/form";
import xhr from "lib/xhr";
import search_template from "widget/search_holder/search_template";
import holder_template from "widget/search_holder/holder_template";

import {atom, swap, deref} from "lib/atom";
import {render} from "lib/template";
import {KEY, trim} from "lib/helpers";


const DEFAULT = {
    "path": null
};

const MIN_TERM_LEN = 2;

const INITIAL_POS = 0;

let a_pos = atom(INITIAL_POS);

let p_dataToOpts = fp.partial(dom.dataToOpts, "search");

let is_valid_term = (term) => {
     return term.length > MIN_TERM_LEN;
};

let sanitize_value = (value) => {
    return trim(value).toLowerCase();
};

let onkeyup = (elem, event) => {
    let dd = dom.find(".b-search-res__list");

    if (!dd) {
        return true;
    }

    const idx = deref(a_pos);

    const last_idx = dd.childNodes.length - 1;

    if ( event.which === KEY.DOWN )  {
        if (idx >= last_idx) {
            swap(a_pos, () => { return INITIAL_POS });
        } else {
            swap(a_pos, fp.inc);
        }

    } else if (event.which === KEY.UP) {
        if (idx === INITIAL_POS) {
            swap(a_pos, () => { return last_idx });
        } else {
            swap(a_pos, fp.dec);
        }

    } else if (event.which === KEY.ESC) {
        dom.remove(dom.find(".b-search-res"));
        form.setval(elem, "header-search", "");
    }
};

export default (search_holder, opts) => {
    if (!search_holder) {
        return;
    }

    let holder_box = dom.find(search_holder,".b-holder__wrap");
    let search = dom.find(search_holder, ".b-search__input");
    let search_elem = dom.find(search_holder, ".b-search");

    opts = fp.merge(DEFAULT, opts);
    swap(a_pos, () => {
        return INITIAL_POS;
    });

    let oldVal = "";

    let sccb = function({data}) {
        dom.remove(dom.find(".b-search-res"));
        let new_item = render(search_template, data);
        dom.append(dom.parent(search), new_item);
     };

    search.onkeyup = (event) => {
        const val = sanitize_value(search.value);

        if (is_valid_term(val) && (oldVal !== val)) {
            xhr.get(opts.path + val, {sccb});
        } else if(!is_valid_term(val) && dom.find(".b-search-res")) {
            dom.remove(dom.find(".b-search-res"));
        } else {
            onkeyup(search_elem,  event);
        }
        oldVal = val;
    };

    dom.listen(search_holder, ".b-search-res__item, .b-search-res__link", "click", (ev) => {
        let elem = ev.target.closest(".b-search-res__item");
        let params = p_dataToOpts(elem);

        let new_item = render(holder_template, params);
        dom.append(holder_box, new_item);

        dom.remove(elem);

        if(dom.findAll(".b-search-res__item").length <= 0) {
            dom.remove(dom.find(".b-search-res"));
            form.setval(dom.find(".b-search"), "header-search", "");
        }
    });

    dom.listen(search_holder, ".b-holder__remove", "click", (ev) => {
        dom.remove(ev.target.closest(".b-holder"));
    });

};

document.body.addEventListener("click", function(ev) {
    let search = dom.findAll(".b-search");
    if(dom.find(".b-search-res") && ev.target.className !== "b-search-res__item b-holder__search-res-item") {
        dom.remove(dom.find(".b-search-res"));
        for(var i=0; i<search.length; i++) {
            form.setval(search[i], "header-search", "");
        }
    }
});
