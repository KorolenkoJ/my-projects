import fp from "lib/fp";
import dom from "lib/dom";
import form from "lib/form";
import xhr from "lib/xhr";
import search_template from "widget/search_holder/search_template";
import holder_template from "widget/search_holder/holder_template";

import {atom, swap, deref} from "lib/atom";
import {render} from "lib/template";
import {KEY, trim} from "lib/helpers";


const DEFAULT = {
    "path": null

}
let search = dom.find(".b-search__input");
let search_elem = dom.find(".b-search");

let p_dataToOpts = fp.partial(dom.dataToOpts, "search");

const MIN_TERM_LEN = 2;

const INITIAL_POS = 0;

let a_pos = atom(INITIAL_POS);


let is_valid_term = (term) => {
     return term.length > MIN_TERM_LEN;
};

let sanitize_value = (value) => {
    return trim(value).toLowerCase();
};


let onkeyup = (event) => {
    let dd = dom.find(".b-search-res__list");

    if (!dd) {
        return true;
    }

    const idx = deref(a_pos);

    const last_idx = dd.childNodes.length - 1;

    if ( event.which === KEY.DOWN )  {
        if (idx >= last_idx) {
            swap(a_pos, () => { return INITIAL_POS });
        } else {
            swap(a_pos, fp.inc);
        }

    } else if (event.which === KEY.UP) {
        if (idx === INITIAL_POS) {
            swap(a_pos, () => { return last_idx });
        } else {
            swap(a_pos, fp.dec);
        }

    } else if (event.which === KEY.ESC) {
        dom.remove(dom.find(".b-search-res"));
        form.setval(search_elem, "header-search", "");
    }

};


export default (search_holder_species, opts) => {
    opts = fp.merge(DEFAULT, opts);

    swap(a_pos, () => {
        return INITIAL_POS;
    });

    let oldVal = "";
    let prevParam = [];

    let sccb = function({data}) {
        dom.remove(dom.find(".b-search-res"));
        let new_item = render(search_template, data);
        dom.append(dom.parent(search), new_item);
     };

    search.onkeyup = (event) => {
        const val = sanitize_value(search.value);

        if (is_valid_term(val) && (oldVal !== val)) {
            xhr.get(opts.path + val, {sccb});
        } else if(!is_valid_term(val) && dom.find(".b-search-res")) {
            dom.remove(dom.find(".b-search-res"));

        } else {
            onkeyup(event);
        }
        oldVal = val;
    };

    dom.listen(search_holder_species, ".b-search-res__item, .b-search-res__link", "click", (ev) => {
        let elem = ev.target.closest(".b-search-res__item");
        let params = p_dataToOpts(elem);


        let new_item = render(holder_template, params);
        dom.addClass(elem, "b-search-res__item_disabled");

        if(prevParam.indexOf(params.id) <= -1) {
            prevParam.push(params.id);
            dom.append(search_holder_species, new_item);
        }
        else {
            return false;
        }

    });

    dom.listen(search_holder_species, ".b-holder__remove", "click", (ev) => {
        dom.remove(ev.target.closest(".b-holder"));
    });
};
