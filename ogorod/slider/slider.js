import fp from "lib/fp";
import dom from "lib/dom";

import {atom, swap, deref} from "lib/atom";
import {KEY} from "lib/helpers";


let nextId = (num, max) => {
    if (num >= max) {
        return num = 0;
    }
    return num+1;
};

let prevId = (num, max) => {
    if (num === 0) {
        return num = max;
    }
    return num-1;
};


export default (slider) => {
    if (!slider) {
        return false;
    }

    let a_state = atom({
        'idx': 0,
        'curHeight': 0,
    });


    let slider_item = dom.findAll(slider, ".b-slider__item");
    let slider_img = dom.findAll(slider, ".b-slider__img");
    let preview_item = dom.findAll(slider, ".b-slider__preview-item");
    let img_cover = dom.find(slider, ".b-gallery__slider-box");
    let preloader = dom.find(slider, ".b-preloader");

    const LENGTH = slider_img.length - 1;

    let toggleClass = (index) => {
        dom.removeClass(dom.find(slider, ".b-slider__item_active"), "b-slider__item_active");
        dom.removeClass(dom.find(slider, ".b-slider__preview-item_active"), "b-slider__preview-item_active");
        dom.addClass(slider_item[index], "b-slider__item_active");
        dom.addClass(preview_item[index], "b-slider__preview-item_active");
    };

    let setHeight = (cur_state) => {
        img_cover.style.height = cur_state.curHeight + "px";
    }

    let handle_img_changed = () => {
        swap(a_state, (current) => {
            return fp.merge(current, {
                'curHeight': slider_img[current.idx].offsetHeight,
            });
        });
        setHeight(deref(a_state));
    };

    let showImg = (index) => {
        swap(a_state, (current) => {
            return fp.merge(current, {'idx': index, 'curHeight': slider_img[index].offsetHeight});
        });

        const state = deref(a_state);

        // If the current image is loaded and have a height => pass a data to corresponding functions
        if (state.curHeight > 0) {
            setHeight(state);
            toggleClass(state.idx);
            preloader.style.display = "none";
        }

        slider_img[state.idx].onload = handle_img_changed;
        toggleClass(index);
    }


    dom.listen(slider, ".b-gallery__slider-box", "click", (ev) => {
        let idx = deref(a_state).idx;
        let cur_el = ev.target.closest(".b-slider__arrow");

        if (dom.hasClass(cur_el, "b-slider__arrow_next")) {
            showImg(nextId(idx, LENGTH));
        }
        else if (dom.hasClass(cur_el, "b-slider__arrow_prev")) {
            showImg(prevId(idx, LENGTH));
        }
    });

    preview_item.forEach(function(item, i) {
        item.addEventListener('click', function() {
            showImg(i);
        });
    });


    document.onkeyup = (ev) => {

        let idx = deref(a_state).idx;
        if (ev.which === KEY.RIGHT){
            showImg(nextId(idx, LENGTH));

        }
        if (ev.which === KEY.LEFT) {
            showImg(prevId(idx, LENGTH));
        }
        if(ev.which === KEY.ESC) {
            window.history.back();
            // return false;
            // console.log(ev);
        }
    };

    window.onresize = handle_img_changed;

    showImg(deref(a_state).idx);
};
