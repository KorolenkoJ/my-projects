import dom from "lib/dom";
import fp from "lib/fp";

let item_template = (item) => {
    return ["li", {"class": "b-search-res__item"},
        ["a", {"class": "b-search-res__link", "href": `${item.link}`}, `${item.title}`]];
};

let template = function(suggest)  {
    return [ "div", {"class": "b-search-res"}, ["ul", {"class": "b-search-res__list"}].concat(suggest.map(item_template)) ];
};

export default template;
