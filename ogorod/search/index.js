import fp from "lib/fp";
import dom from "lib/dom";
import form from "lib/form";
import xhr from "lib/xhr";
import template from "widget/search/template";

import {atom, swap, deref} from "lib/atom";
import {render} from "lib/template";
import {KEY, trim} from "lib/helpers";


const MIN_TERM_LEN = 2;

const INITIAL_POS = 0;

const DEFAULT = {
    "path": "/api/v1/suggest/",
};

let a_pos = atom(INITIAL_POS);

let is_valid_term = (term) => {
     return term.length >= MIN_TERM_LEN;
};

let sanitize_value = (value) => {
    return trim(value).toLowerCase();
};

let onkeyup = (event) => {
    let dd = dom.find(".b-search-res__list");

    if (!dd) {
        return true;
    }

    const idx = deref(a_pos);

    const last_idx = dd.childNodes.length - 1;
    dom.removeClass(dom.find(".b-search-res__item_focus"), "b-search-res__item_focus");

    if ( event.which === KEY.DOWN )  {
        if (idx >= last_idx) {
            swap(a_pos, () => { return INITIAL_POS });
        } else {
            swap(a_pos, fp.inc);
        }
        // console.log(a_pos, idx);

    } else if (event.which === KEY.UP) {
        if (idx === INITIAL_POS) {
            swap(a_pos, () => { return last_idx });
        } else {
            swap(a_pos, fp.dec);
        }

    } else if (event.which === KEY.ENTER) {
        location.href = dd.childNodes[idx].firstChild.getAttribute("href");

    } else if (event.which === KEY.ESC) {
        dom.remove(dom.find(".b-search-res"));
        form.setval(dom.find(".b-search"), "header-search", "");
    }

    dom.addClass(dd.childNodes[deref(a_pos)], "b-search-res__item_focus");
};


export default (search, opts) => {
    if(!search) {
        return true;
    }

    opts = fp.merge(DEFAULT, opts);

    swap(a_pos, () => {
        return INITIAL_POS;
    });

    let oldVal = "";

    let sccb = function({data}) {
        dom.remove(dom.find(".b-search-res"));
        let new_item = render(template, data);
        dom.append(dom.parent(search), new_item);
        dom.addClass(dom.find(".b-search-res__list").childNodes[0], "b-search-res__item_focus");
    };

    search.onkeyup = (event) => {
        const val = sanitize_value(search.value);

        if (is_valid_term(val) && (oldVal !== val)) {
            xhr.get(opts.path + val, {sccb});
        } else if(!is_valid_term(val) && dom.find(".b-search-res")) {
            dom.remove(dom.find(".b-search-res"));

        } else {
            onkeyup(event);
        }
        oldVal = val;
    };
};
