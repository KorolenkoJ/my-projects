import Vue from 'vue'
import App from './App.vue'
import './settings'


const app = new Vue(App).$mount('#app')
