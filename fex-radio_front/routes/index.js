export default [
    {
        path: '/genres',
        name: 'genres',
        component: require('./../components/genres/genres.vue').default
    },
    {
        path: '/stations',
        name: 'stations',
        component: require('./../components/stations/stations.vue').default
    },
    {
        path: '/station/:id',
        name: 'station',
        component: require('./../components/station/station.vue').default
    },
        {
        path: '/new_station/',
        name: 'new_station',
        component: require('./../components/new_station/new_station.vue').default
    },
    {
        path: '/auth',
        name: 'auth',
        component: require('./../components/auth/auth.vue').default
    },
];
