import Vue from 'vue';
import Axios from "axios";

Vue.config.debug = process.env.NODE_ENV !== "production";

Vue.prototype.$http = Axios;


// Axios.defaults.baseURL = "https://fon.fex.net/api/v1";
Axios.defaults.headers.post['Content-Type'] = 'application/json';

Object.defineProperty(Vue.prototype, "$http", {
  get() {
    return Axios;
  }
});

require('./assets/sass/app.sass');

/* ============
 * Vuex
 * ============
 */
import Vuex from 'vuex';
Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    isLogin: false,
    userName: '',
  },
  mutations: {
    toggleLogin (state) {
        if(localStorage.getItem("token")) {
            state.isLogin = true;
        }
        else {
            state.isLogin = false;
        }
    },
    setUserName(state, name) {
        state.userName = name;
    },
  }
});

Vue.prototype.$store = store;

const validation = {

  checkForm(form, container) {
    let msg = this.msgError(),
        elemsCompared = [],
        allow = true;

    for(let key in form) {

      if(form[key].dataset && form[key].name) {

        if(form[key].dataset.required) {
          if( !this.checkRequire(form[key], container, msg) ) {
              allow = false;
              continue;
          }
        }
        if(form[key].dataset.length) {
          if( !this.checkLength(form[key], form[key].dataset.length, container, msg, allow) ) {
              allow = false;
              continue;
          }
        }
        if(form[key].dataset.compare) {
          let elem = document.getElementsByName(form[key].dataset.compare)[0];
          elemsCompared.push(form[key]);
          if( !this.compareValue(elem, form[key], elemsCompared, container, msg, allow) ) {
              allow = false;
              continue;
          }
        }
      }

    }
    return allow;

  },

  creatMsg(container, elem, txt) {
      let span = document.createElement("span");
      span.setAttribute("class", "b-msg__error");
      container.appendChild(span);
      span.innerHTML = txt;

      let fields = elem.closest(".js-valid-block").getElementsByClassName("js-form-valid");

      for(let i=0; i<fields.length; i++) {
        fields[i].classList.add("b-input__error");
      }
  },

  msgError() {
    let errors = {
      require: "This field is required",
      long: "The station name can't contain more than 32 characters",
      sameValue: "This fields can not have the same values",
    }
    return errors;
  },

  clearError(elem) {
    let fields = elem.closest(".js-valid-block").getElementsByClassName("js-form-valid");

    if(elem.nextSibling) {
        elem.nextSibling.remove();

        for(let i=0; i<fields.length; i++) {
          fields[i].classList.remove("b-input__error");
        }
    }
  },

  checkRequire(elem, container, msg) {
    this.clearError(elem);

    if(elem.value.length > 0) {
      return true;
    }

    this.creatMsg(elem.closest(container), elem, msg.require);
    return false;

  },

  checkLength(elem, length, container, msg) {
    this.clearError(elem);

    if( (elem.value.length > 0) && (elem.value.length <= length) ) {
        return true;
    }

    this.creatMsg(elem.closest(container), elem, msg.long);
    return false;
  },

  compareValue(elem1, elem2, arr, container, msg) {

    if( (elem1.value && elem2.value) && (elem1.value.toUpperCase() === elem2.value.toUpperCase()) ) {
      for(let key in arr) {
        this.clearError(arr[key]);
        this.creatMsg(arr[key].closest(container), arr[key], msg.sameValue);
      }
      return false;
    }

    for(let key in arr) {
        this.clearError(arr[key]);
    }
    return true;

    // else if( elem1.value.toUpperCase() !== elem2.value.toUpperCase() ) {
    //   for(let key in arr) {
    //       this.clearError(arr[key]);
    //   }
    //   return true;
    // }
  }

}

Vue.prototype.$validation = validation;


/* ============
 * Vue Router
 * ============
 * http://router.vuejs.org/en/index.html
 */
import VueRouter from 'vue-router'
import routes from './routes';

Vue.use(VueRouter);



// import { ContainerMixin, ElementMixin } from 'vue-slicksort';

// let mixins = {ContainerMixin: ContainerMixin, ElementMixin: ElementMixin};
// Vue.prototype.$mixins = mixins;

export const router = new VueRouter({
  routes,
});


export default {
  router,
};


