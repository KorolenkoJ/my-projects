export default {
    mixins: [],

    data: () => ({
        stationsList: [],
        sortList: {},
        indexList: [],
        errors: [],
        config: {
            headers: {
                // 'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1IjoxNTc3MTEsInAiOjMyLCJpYXQiOjE1MjM1MzYxNzAsImV4cCI6MTUyNjEyODE3MH0.zunuJx8oM4Vw-DrO_BkuJVZ9FTBHHe4384g7jSllLFk',
                'Authorization': 'Bearer ' + localStorage.getItem("token"),
            }
        }
    }),

    created() {

        this.$http.get('/api/v1/station/images/get/all', this.config)
            .then(response => {
                this.stationsList = response.data;

                // function compareNumeric(a, b) {
                //   if (a > b) return 1;
                //   if (a < b) return -1;
                // }

                // this.drop();

        })
        .catch(e => {
            this.errors.push(e)
        });

    },
    mounted() {
        document.documentElement.classList.remove("b-html__menu-open");
    },
    methods: {
        drop() {
            let source;
            let drop_block = this.$refs.drop_block;
            let curObj = this.stationsList;
            let self = this;

            function isbefore(a, b) {
                if (a.parentNode == b.parentNode) {
                    for (var cur = a; cur; cur = cur.previousSibling) {
                        if (cur === b) {
                            return true;
                        }
                    }
                }
                return false;
            };
            drop_block.addEventListener("dragstart", function( event ) {
                source = event.target.closest(".draggable");
                event.dataTransfer.effectAllowed = 'move';
                event.dataTransfer.setData('text/html', event.target.id);

            }, false);

            drop_block.addEventListener("dragenter", function( event ) {
                if (isbefore(source, event.target.closest(".draggable"))) {
                    event.target.closest(".dropzone").insertBefore(source, event.target.closest(".draggable"));
                }
                else {
                    event.target.closest(".dropzone").insertBefore(source, event.target.closest(".draggable").nextSibling);
                }
                 source.style.background = '#d6edf7';

            }, false);

            drop_block.addEventListener("dragend", function( event ) {
                source.style.background = '';

                self.stationsList.data.forEach(function(item, i) {
                    // item.station_index = i+1;
                    console.log(item.station_index, item.name);
                });

                self.sortObj();

            }, false);
        },

        sortObj() {
            // for(let key in this.stationsList) {
            //     console.log(curObj[key].station_index, key);
            // }
            let collection = document.getElementsByClassName("draggable");

            for(let i=0; i<collection.length; i++) {
                // collection[i];
                // console.log(collection[i]);
                // this.stationsList.data[i].station_index = i+1;
            }

        },

        saveOrder(event) {
            // let stations = document.getElementsByClassName("draggable");

            // for(let i=0; i<stations.length; i++) {
            //     this.indexList[stations[i].id] = i+1;
            // }
            // this.sortList["index_dict"] = this.indexList;

            this.$http({'url': '/api/v1/station/indexes/update', 'method': 'PUT', 'data': this.sortList, 'headers': this.config.headers})
                .then(response => {
                    // for(let key in this.stationsList.data) {
                    //     console.log(this.stationsList.data[key]);

                    // }
                        // console.log(key);
                    // this.stationsList = response.data;
                })
                .catch(e => {
                    this.errors.push(e)
            });
        },

        remove(event) {
            let station = event.target.closest(".b-table__row");
            // let station_id = event.target.closest(".b-table__row").id;
            // console.log(station_id);
            this.$http.delete('/api/v1/station/delete/' + station.id, this.config)
                .then(response => {
                    station.remove();
            })
            .catch(e => {
                this.errors.push(e);
            });
        }

    }
}
