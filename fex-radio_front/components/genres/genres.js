
export default {
    data: () => ({
        genresList: [],
        errors: [],
        config: {
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem("token"),
            }
        }
    }),

    created() {
        this.$http.get('/api/v1/genre/get/all', this.config)
            .then(response => {
                this.genresList = response.data
        })
        .catch(e => {
            this.errors.push(e)
        });
    },
    mounted() {
        document.documentElement.classList.remove("b-html__menu-open");
    }
}
