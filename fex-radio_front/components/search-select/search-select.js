export default {
    props: ["allGenres", "stationGenres"],

    data: () => ({
        errors: [],
        config: {
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem("token"),
            }
        },
        dd: null,
        genresSelect: [],
        list_item: null,
        errorClass: true,
        idx: 0,
        input: null,
        holder_box: null,
    }),
    mounted(event) {

        let holder_item;
        this.input = this.$refs.searchSelectInput;
        this.holder_box = this.$refs.holderBox;
        this.list_item = document.getElementsByClassName("b-dd__item");
        this.dd = document.getElementsByClassName("b-dd")[0];

        if(!this.stationGenres) {
            return false;
        }

        for(let i=0; i<this.stationGenres.length; i++) {

            holder_item = this.creatItem(this.stationGenres[i].name, this.stationGenres[i].genre_id);

            this.genresSelect.push(this.stationGenres[i].genre_id + "");

            this.holder_box.appendChild(holder_item);
        }
    },

    methods: {
        dropDown(event) {

            let self = this;

            this.dd.style.display = "block";

            this.input.value = "";

            for(let i=0; i<this.list_item.length; i++) {

                for(let k=0; k<this.holder_box.childNodes.length; k++) {

                    if(this.holder_box.childNodes[k].id === this.list_item[i].id) {
                        this.list_item[i].classList.add("b-dd__item_inactive");
                    }
                }
            }

            this.dd.addEventListener("click", function(event) {

                if(!event.target.classList.contains("b-dd__item_inactive")) {

                    self.input.value = event.target.getAttribute("data-name");
                    event.target.classList.add("b-dd__item_inactive");

                    let holder_item = self.creatItem(self.input.value, event.target.id);

                    self.holder_box.appendChild(holder_item);

                    self.genresSelect.push(event.target.id);

                    self.$emit('getGenres', {
                        genresSelect: self.genresSelect
                    });
                }
            });
        },
        navigate(event) {
            let dd = document.getElementsByClassName("b-dd")[0];
            let list_item = document.getElementsByClassName("b-dd__item");
            let last_idx = list_item.length;
            let num = this.idx;



            // console.log(event.keyCode);

            // for(let i=0; i<list_item.length; i++) {
            //     list_item[i].classList.remove("b-dd__item_focus");
            // }

            // list_item[num].classList.add("b-dd__item_focus");
            // // console.log(this.idx);

            // if(event.which === 40) {
            //     if(this.idx >= last_idx) {
            //         return this.idx = 0;
            //     }
            //     else {
            //         this.idx += 1;
            //     }
            // }
            // if(event.which === 38) {
            //     if(this.idx < 0 ) {
            //         return this.idx = last_idx;
            //     }
            //     else {
            //         this.idx -= 1;
            //     }
            // }

            // console.log(num);
            // console.log(this.idx);

        },
        search(event) {
            let inputVal = event.target.value;

            inputVal = inputVal.toUpperCase();

            for(let i=0; i<this.list_item.length; i++) {
                if(this.list_item[i].innerHTML.toUpperCase().indexOf(inputVal) > -1) {
                    this.list_item[i].style.display = "";
                }
                else {
                    this.list_item[i].style.display = "none";
                }
            }
        },
        creatItem(value, id) {
            let holder_item = document.createElement("div");

            holder_item.setAttribute("class", "b-holder__item b-holder__close");
            holder_item.setAttribute("data-name", value);
            holder_item.setAttribute("id", id);

            holder_item.innerHTML = value;
            return holder_item;
        },
        removeItems(event) {

            for(let i=0; i<this.list_item.length; i++) {
                if(event.target.id == this.list_item[i].id) {
                    this.list_item[i].classList.remove("b-dd__item_inactive");

                    this.genresSelect = this.genresSelect.filter(val => val !== event.target.id);

                    event.target.remove();

                    this.$emit('getGenres', {
                        genresSelect: this.genresSelect
                    });
                }
            }
            this.input.value = "";
        }
    }
}



