export default {
    data: () => ({
        errors: [],
        config: {
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem("token"),
            }
        },
    }),
    mounted() {
        document.documentElement.classList.remove("b-html__menu-open");
    },

    methods: {
        logIn(event) {
            let form = new FormData(event.target);

            this.$store.commit('setUserName', form.get('phone'));

            if( form.get('phone').length < 4 || form.get('password').length < 4 ) {
                return false;
            }

            this.$http({'url': event.target.action, 'method': event.target.method, 'data': form})
                .then(response => {
                    localStorage.setItem("token", response.data.token);
                    localStorage.setItem("phone", form.get('phone'));
                    this.$store.commit('toggleLogin');
                    this.$router.push({ path: '/stations'});
                })
                .catch(e => {
                    this.errors.push(e)
            });
        },
        logOut() {
            this.$http.delete('/api/v1/user/auth/logout', this.config)
            .then(response => {})
            .catch(e => {
                this.errors.push(e)
                localStorage.removeItem("token");
                this.$store.commit('toggleLogin');

                if (e.response.status >= 300 && e.response.status < 400) {
                    this.$router.push({ path: e.response.data.location + 'auth'});
                }
            });
        },
    }

}

