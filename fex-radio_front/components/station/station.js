import AppImageUpload from '../image-upload/image-upload.vue';
import AppSearchSelect from "../search-select/search-select.vue";

export default {
    components: {
        AppImageUpload,
        AppSearchSelect,
    },
    props: ["test"],

    data: () => ({
        stationData: [],
        errors: [],
        all_genres: [],
        stationInfo: {
            name: '',
            imgUrl: '',
            id: null,
            station_genres_obj: [],
            station_genres_list: [],
            stream_links: []
        },

        formEl: null,
        dd: null,
        block_notification: null,

        config: {
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem("token"),
            }
        }
    }),

    created() {
        this.$http.get('/api/v1/station/get/' + this.$route.params.id, this.config)
            .then(response => {
                this.stationData = response.data;
                this.stationInfo.name = response.data.name;
                this.stationInfo.imgUrl = response.data.image_url;
                this.stationInfo.station_genres_obj = response.data.genres;
                this.stationInfo.input_stream = response.data.input_stream;
                this.stationInfo.output_stream = response.data.output_stream;
                this.stationInfo.id = response.data.id;

                for(let i=0; i<response.data.genres.length; i++) {
                    this.stationInfo.station_genres_list.push(response.data.genres[i].genre_id);
                }

        })
        .catch(e => {
            this.errors.push(e)
            if (e.response.status == 404) {
                this.stationInfo.imgUrl = 'http://s1.iconbird.com/ico/1012/Qetto2Icons/w256h2561350657667radio.png';
            }
        });

        this.$http.get("/api/v1/genre/get/all", this.config)
            .then(response => {
                this.all_genres = response.data;
        })
        .catch(e => {
            this.errors.push(e)
        });
    },
    mounted(event) {
        this.formEl = document.forms.editStation;
        this.windowEvents(event, this.formEl);
        document.documentElement.classList.remove("b-html__menu-open");
    },

    methods: {
        changeImgUrl(newUrl) {
            this.stationInfo.imgUrl = newUrl.src;
            this.stationInfo.id = newUrl.id;
        },
        genres(getGenres) { // Getting parameters from a child element
            this.stationInfo.station_genres_list = getGenres.genresSelect;
        },
        windowEvents(event, formEl) {

            let container = this.$refs.container;
            let b_holders = document.getElementsByClassName("b-holder__item");
            let dd = null;

            container.addEventListener("click", function(event) { // Close genres list on click outside
                dd = document.getElementsByClassName("b-dd")[0];

                if( !event.target.classList.contains("b-dd__item")
                        && !event.target.classList.contains("b-holder__item")
                        && !event.target.classList.contains("js-search-select")
                        && dd ) {
                    dd.style.display = "none";
                }
            });

            document.body.addEventListener("keyup", function(event) { // Close genres list on ESC click
                dd = document.getElementsByClassName("b-dd")[0];
                if(event.which === 27 && dd) {
                    dd.style.display = "none";
                }
            })
        },
        save(event) {

            let form = new FormData(event.target);
            let sendData = {};
            let success_notification = "The Station is saved";

            sendData.name = form.get("name");
            sendData.image_id = this.stationInfo.id;
            sendData.input_stream = {"url": form.get("input_stream")};
            sendData.output_stream = {"url": form.get("output_stream")};
            sendData.genres = this.stationInfo.station_genres_list;

            // console.log(sendData);


            if( this.$validation.checkForm(document.forms.editStation.elements, ".js-valid-block") ) {  // Send request if the front validation return true
                this.$http({"url": event.target.action, "method": event.target.getAttribute("method"), "data": sendData, "headers": this.config.headers})
                    .then(response => {
                         // setTimeout( () => { this.notification(success_notification, this.$refs.container) }, 700);
                         this.notification(success_notification, this.$refs.container);
                    })
                    .catch(e => {
                        this.errors.push(e);
                        let fields = e.response.data.form;

                        for(let key in fields) {

                            if( e.response.status == 400) {
                                    this.$validation.creatMsg(this.formEl.elements[key].closest(".b-input__block"), this.formEl.elements[key], "Such value already exists");
                            }
                            else {
                                this.formEl.elements[key].nextSibling.remove();
                                this.formEl.elements[key].classList.remove("b-input__error");
                            }
                        }
                });
            }
        },
        remove() {
            this.$http.delete('/api/v1/station/delete/' + this.$route.params.id, this.config)
                .then(response => {
                    this.$router.push({ path: '/stations'});
            })
            .catch(e => {
                this.errors.push(e);
            });
        },
        notification(msg, container) {
            if(!this.block_notification) {
                this.block_notification = document.createElement("div");
                this.block_notification.setAttribute("class", "b-notification");
            }

            this.block_notification.innerHTML = msg;
            container.appendChild(this.block_notification);
            return this.block_notification;
        }
    }
}
