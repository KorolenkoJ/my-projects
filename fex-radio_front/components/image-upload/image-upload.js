export default {
    props: ['src'],

    data: () => ({
        errors: [],
        config: {
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem("token"),
            }
        },
    }),

    methods: {

        imgLoad(target) {
            let inp = document.createElement('input');
            let img;
            inp.setAttribute('type', 'file');
            inp.setAttribute('class', 'b-input__hidden');
            target.parentElement.insertBefore(inp, target);
            inp.click();
            inp.addEventListener('change', (event) => {

                let formData = new FormData();
                formData.append('upload', inp.files[0]);

                this.$http.post('/api/v1/image/upload', formData, this.config)
                    .then(response => {
                        this.$emit('newUrl', {
                            src: response.data.url,
                            id: response.data.id
                        });
                })
                .catch(e => {
                    this.errors.push(e);
                });

            });
            target.previousSibling.remove();
        },
    }
}



