import AppImageUpload from "../image-upload/image-upload.vue";
import AppSearchSelect from "../search-select/search-select.vue";

export default {
    components: {
        AppImageUpload,
        AppSearchSelect
    },

    data: () => ({
        stationData: {
            name: null,
            genres: [],

        },
        errors: [],
        genresList: [],
        stationInfo: {
            name: "",
            imgUrl: "http://s1.iconbird.com/ico/1012/Qetto2Icons/w256h2561350657667radio.png",
        },
        config: {
            headers: {
                "Authorization": "Bearer " + localStorage.getItem("token"),
            }
        },
        formEl: null,
        dd: null
    }),
    created() {
        this.$http.get("/api/v1/genre/get/all", this.config)
            .then(response => {
                this.genresList = response.data;
        })
        .catch(e => {
            this.errors.push(e)
        });
    },
    mounted(event) {
        this.formEl = document.forms.addStation;
        this.windowEvents(event, this.formEl);
        document.documentElement.classList.remove("b-html__menu-open");
    },

    methods: {
        changeImgUrl(newUrl) { // Getting parameters from a child element
            this.stationInfo.imgUrl = newUrl.src;
            this.stationInfo.id = newUrl.id;
        },
        genres(getGenres) { // Getting parameters from a child element
            this.stationData.genres = getGenres.genresSelect;
        },
        save(event) {
            let form = new FormData(event.target);

            this.stationData.name = form.get("name");
            this.stationData.image_id = this.stationInfo.id;
            this.stationData.input_stream = {"url": form.get("input_stream")};
            this.stationData.output_stream = {"url": form.get("output_stream")};


            if( this.$validation.checkForm(document.forms.addStation.elements, ".js-valid-block") ) {  // Send request if the front validation return true

                this.$http({"url": event.target.action, "method": event.target.getAttribute("method"), "data": this.stationData, "headers": this.config.headers})
                    .then(response => {
                        this.$router.push({ path: '/stations'});
                    })
                    .catch(e => {
                        this.errors.push(e);
                        let fields = e.response.data.form;

                        for(let key in fields) {

                            if( e.response.status == 400) {
                                    this.$validation.creatMsg(this.formEl.elements[key].closest(".b-input__block"), this.formEl.elements[key], "Such value already exists");
                            }
                            else {
                                this.formEl.elements[key].nextSibling.remove();
                                this.formEl.elements[key].classList.remove("b-input__error");
                            }
                        }
                });
            }
        },

        windowEvents(event, formEl) {

            let container = this.$refs.container;
            let b_holders = document.getElementsByClassName("b-holder__item");
            let dd = null;

            container.addEventListener("click", function(event) { // Close genres list on click outside
                dd = document.getElementsByClassName("b-dd")[0];

                if( !event.target.classList.contains("b-dd__item")
                        && !event.target.classList.contains("b-holder__item")
                        && !event.target.classList.contains("js-search-select")
                        && dd ) {
                    dd.style.display = "none";
                }
            });

            document.body.addEventListener("keyup", function(event) { // Close genres list on ESC click
                dd = document.getElementsByClassName("b-dd")[0];
                if(event.which === 27 && dd) {
                    dd.style.display = "none";
                }
            })
        }
    },
}
